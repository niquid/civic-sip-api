package civic

import (
	"math/big"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSIPPublicKey(t *testing.T) {
	pubKey := SIPPublicKey()

	x, _ := new(big.Int).SetString("69779150715099576753547071627675990503491497823279796025979531157683662006444", 10)
	y, _ := new(big.Int).SetString("39366605134374098802012617860418124118774606238640623774316610014162243971745", 10)
	assert.Equal(t, x, pubKey.X)
	assert.Equal(t, y, pubKey.Y)
}
