package jwt

import (
	"crypto/hmac"
	"crypto/sha256"
	"hash"
)

func hashData(h hash.Hash, data []byte) []byte {
	h.Write(data)
	return h.Sum(nil)
}

func sha256Hash(data []byte) []byte {
	return hashData(sha256.New(), data)
}

func hmacSha256Hash(data, secret []byte) []byte {
	return hashData(hmac.New(sha256.New, secret), data)
}
