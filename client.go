package civic

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/pkg/errors"
	"go.niquid.tech/civic-sip-api/aes"
	"go.niquid.tech/civic-sip-api/jwt"
)

const (
	jwtExpiration = 3 * time.Minute
)

// The Client is the Civic SPI API client
type Client struct {
	Config Config
	HTTP   *http.Client
}

// UserRecord hold the user id and its scope request data
type UserRecord struct {
	UserID string   `json:"userId"`
	Data   UserData `json:"data"`
}

// UserData is a list of entries of the scope request
type UserData []struct {
	Label   string      `json:"label"`
	Value   interface{} `json:"value"`
	IsValid bool        `json:"isValid"`
	IsOwner bool        `json:"isOwner"`
}

// ScopeRequestData is the encrypted data returned to the server
type ScopeRequestData struct {
	Data      string `json:"data"`
	UserID    string `json:"userId"`
	Processed bool   `json:"processed"`
	Encrypted bool   `json:"encrypted"`
	Algo      string `json:"alg,omitempty"`
}

// NewClient creates a new client with a default HTTP client
func NewClient(config Config) (*Client, error) {
	client := &Client{
		Config: config,
		HTTP:   &http.Client{Timeout: 15 * time.Second},
	}
	return client, nil
}

// ExchangeCode gets user data for the provided authorization code (JWT token)
func (c *Client) ExchangeCode(authCode string) (*UserRecord, error) {
	data, err := c.RetrieveScopeRequestData(authCode)
	if err != nil {
		return nil, errors.Wrap(err, "failed to exchange code for user record")
	}

	return c.ExtractUserRecord(data)
}

// RetrieveScopeRequestData gets the raw (undecrypted) user scope data
func (c *Client) RetrieveScopeRequestData(authCode string) (*ScopeRequestData, error) {
	req, err := c.prepareExchangeCodeRequest(authCode)
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare request")
	}

	var data ScopeRequestData
	err = DoJSON(c.HTTP, req, &data)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get json")
	}

	if data.Processed {
		err = c.fetchProcessedData(&data)
	}
	return &data, err
}

// ExtractUserRecord decodes/decrypts the scope request data and extracts the user record
func (c *Client) ExtractUserRecord(resp *ScopeRequestData) (*UserRecord, error) {
	payload, err := jwt.VerifyAndExtractPayload(resp.Data, &c.Config.SIPPublicKey)
	if err != nil {
		return nil, errors.Wrap(err, "failed to verify and extract record data")
	}
	data, ok := payload.Data.(string)
	if !ok {
		return nil, errors.New("invalid record data")
	}

	if resp.Encrypted {
		data, err = aes.DecryptCivicString(data, c.Config.AppSecret)
		if err != nil {
			return nil, errors.Wrap(err, "invalid encrypted data")
		}
	}

	var userData UserData
	err = json.Unmarshal([]byte(data), &userData)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal user data")
	}

	return &UserRecord{
		UserID: resp.UserID,
		Data:   userData,
	}, nil
}

func (c *Client) fetchProcessedData(data *ScopeRequestData) error {
	resp, err := c.HTTP.Get(data.Data)
	if err != nil {
		return errors.Wrap(err, "failed to http get processed data")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "failed to read processed data")
	}
	data.Data = string(body)
	return nil
}

func (c *Client) prepareExchangeCodeRequest(token string) (*http.Request, error) {
	method, path := "POST", "scopeRequest/authCode"
	body, _ := json.Marshal(map[string]interface{}{
		"authToken":              token,
		"allowS3UploadedPayload": true,
	})
	authHeader, err := c.makeAuthorizationHeader(method, path, body)
	if err != nil {
		return nil, errors.Wrap(err, "failed to make an authorization header")
	}
	url := fmt.Sprintf("%s%s/%s", c.Config.APIBaseURL, c.Config.Env, path)
	req, err := http.NewRequest(method, url, bytes.NewReader(body))
	if err != nil {
		return nil, errors.Wrapf(err, "failed to make a POST request to %s", url)
	}
	req.Header.Set("User-Agent", "") // Otherwise, the client adds a default UA
	req.Header.Set("Authorization", authHeader)
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Content-Type", "application/json")
	return req, nil
}

func (c *Client) makeAuthorizationHeader(method, path string, body []byte) (string, error) {
	data := struct {
		Method string `json:"method"`
		Path   string `json:"path"`
	}{method, path}
	tok, err := jwt.Create(c.Config.AppID, c.Config.APIBaseURL, c.Config.AppID, jwtExpiration, data, &c.Config.PrivateKey)
	if err != nil {
		return "", errors.Wrap(err, "failed to make JWT token")
	}
	ext := jwt.CreateCivicExt(body, c.Config.AppSecret)
	return fmt.Sprintf("Civic %s.%s", tok, ext), nil
}
