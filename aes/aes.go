package aes

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"

	"github.com/pkg/errors"
)

var (
	// ErrInvalidData indicates bad input data
	ErrInvalidData = errors.New("invalid data")
	// ErrInvalidPadding indicates bad padding data
	ErrInvalidPadding = errors.New("invalid padding")
)

// Encrypt uses AES-CBC w/ PKCS7 padding to encrypt a message
func Encrypt(msg, key []byte) ([]byte, []byte, error) {
	iv := make([]byte, 16)
	_, err := rand.Read(iv)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to generate random IV")
	}
	ciphertext, err := EncryptWithIV(msg, key, iv)
	return iv, ciphertext, err
}

// EncryptWithIV allows you to encrypt a message using a specific IV
func EncryptWithIV(msg, key, iv []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create AES cipher")
	}
	plaintext := pkcs7Pad(msg, aes.BlockSize)
	ciphertext := make([]byte, len(plaintext))
	enc := cipher.NewCBCEncrypter(block, iv)
	enc.CryptBlocks(ciphertext, plaintext)
	return ciphertext, nil
}

// EncryptCivicString encrypt message using Civic scheme for encoding
func EncryptCivicString(msg string, key []byte) (string, error) {
	iv, ciphertext, err := Encrypt([]byte(msg), key)
	if err != nil {
		return "", errors.Wrap(err, "failed to encrypt message")
	}
	encrypted := fmt.Sprintf("%s%s", hex.EncodeToString(iv), base64.StdEncoding.EncodeToString(ciphertext))
	return encrypted, nil
}

// Decrypt uses AES-CBC w/ PKCS7 padding to decrypt a message
func Decrypt(iv, ciphertext, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create AES cipher")
	}
	plaintext := make([]byte, len(ciphertext))
	dec := cipher.NewCBCDecrypter(block, iv)
	dec.CryptBlocks(plaintext, ciphertext)
	msg, err := pkcs7Unpad(plaintext, aes.BlockSize)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unpad plaintext")
	}
	return msg, nil
}

// DecryptCivicString decodes encrypted message by Civic
func DecryptCivicString(data string, key []byte) (string, error) {
	iv, err := hex.DecodeString(data[:aes.BlockSize*2])
	if err != nil {
		return "", errors.Wrap(err, "failed to decode IV")
	}
	ciphertext, err := base64.StdEncoding.DecodeString(data[aes.BlockSize*2:])
	if err != nil {
		return "", errors.Wrap(err, "failed to decode base64 ciphertext")
	}
	msg, err := Decrypt(iv, ciphertext, key)
	if err != nil {
		return "", errors.Wrap(err, "faile to decrypt ciphertext")
	}
	return string(msg), nil
}

func pkcs7Pad(data []byte, blockSize int) []byte {
	n := blockSize - (len(data) % blockSize)
	pad := bytes.Repeat([]byte{byte(n)}, n)
	return append(data, pad...)
}

func pkcs7Unpad(data []byte, blockSize int) ([]byte, error) {
	if len(data)%blockSize != 0 {
		return nil, ErrInvalidData
	}

	i := len(data) - 1
	padByte := data[i]
	padStart := i - int(padByte) + 1

	for ; i >= padStart && data[i] == padByte; i-- {
	}
	if i != padStart-1 {
		return nil, ErrInvalidPadding
	}

	return data[:padStart], nil
}
