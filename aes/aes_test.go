package aes

import (
	"bytes"
	"crypto/aes"
	"encoding/base64"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncrypt(t *testing.T) {
	key := bytes.Repeat([]byte{1}, 16)
	msg := []byte("test123")
	iv, ciphertext, err := Encrypt(msg, key)
	assert.NoError(t, err)
	if assert.NotNil(t, iv) {
		assert.Len(t, iv, aes.BlockSize)
	}
	if assert.NotNil(t, ciphertext) {
		assert.Len(t, ciphertext, aes.BlockSize)
	}
}

func TestEncryptString(t *testing.T) {
	key := bytes.Repeat([]byte{1}, 16)
	ciphertext, err := EncryptCivicString("test123", key)
	assert.NoError(t, err)
	assert.NotNil(t, ciphertext)
	assert.Len(t, ciphertext, 2*aes.BlockSize+base64.StdEncoding.EncodedLen(16))
}

func TestDecrypt(t *testing.T) {
	key := bytes.Repeat([]byte{1}, 16)
	iv := []byte{
		144, 44, 69, 117, 155, 166, 29, 50, 164, 188, 173, 95, 255, 141, 182, 52,
	}
	ciphertext := []byte{
		200, 78, 85, 202, 67, 162, 124, 151, 241, 214, 43, 231, 65, 201, 9, 158,
	}
	msg, err := Decrypt(iv, ciphertext, key)
	assert.NoError(t, err)
	if assert.NotNil(t, msg) {
		assert.Equal(t, []byte("test123"), msg)
	}
}

func TestDecryptString(t *testing.T) {
	key := bytes.Repeat([]byte{1}, 16)
	ciphertext := "abfcd56f17962949454245e9c8c80727U6I2WVRQBouBw+D9pxrfoA=="
	msg, err := DecryptCivicString(ciphertext, key)
	assert.NoError(t, err)
	if assert.NotNil(t, msg) {
		assert.Equal(t, "test123", msg)
	}
}

func TestEncryptFails(t *testing.T) {
	t.Run("invalid key", func(t *testing.T) {
		_, _, err := Encrypt(bytes.Repeat([]byte{1}, 32), []byte{1, 2, 3})
		if assert.Error(t, err) {
			assert.Regexp(t, "invalid key size", err)
		}
	})
	t.Run("invalid key string", func(t *testing.T) {
		ciphertext := "902c45759ba61d32a4bcad5fff8db634c84e55ca43a27c97f1d62be741c9099e"
		_, err := EncryptCivicString(ciphertext, []byte{1, 2, 3})
		if assert.Error(t, err) {
			assert.Regexp(t, "invalid key size", err)
		}
	})

}

func TestDecryptFails(t *testing.T) {
	t.Run("invalid key", func(t *testing.T) {
		_, err := Decrypt(bytes.Repeat([]byte{1}, 16), bytes.Repeat([]byte{1}, 16), []byte{1, 2, 3})
		if assert.Error(t, err) {
			assert.Regexp(t, "invalid key size", err)
		}
	})
	t.Run("invalid key string", func(t *testing.T) {
		ciphertext := "902c45759ba61d32a4bcad5fff8db634c84e55ca43a27c97f1d62be741c9099e"
		_, err := DecryptCivicString(ciphertext, []byte{1, 2, 3})
		if assert.Error(t, err) {
			assert.Regexp(t, "invalid key size", err)
		}
	})
}

func TestPkcs7UnpadFails(t *testing.T) {
	t.Run("data does not match blocksize", func(t *testing.T) {
		_, err := pkcs7Unpad([]byte{1, 2, 3, 4, 5, 6, 7, 8, 9}, 16)
		assert.EqualError(t, err, ErrInvalidData.Error())
	})
	t.Run("invalid padding", func(t *testing.T) {
		_, err := pkcs7Unpad(
			[]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
			16,
		)
		assert.EqualError(t, err, ErrInvalidPadding.Error())
	})
}
