package jwt

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
)

const (
	p256KeySize = 32
)

//
const (
	headerIdx    = 0
	payloadIdx   = 1
	signatureIdx = 2
)

// Exported errors
var (
	ErrInvalidCurve = errors.New("invalid curve")
)

// Token is a JSON Web Token type specific for the Civic client
type Token struct {
	parts []string
}

// Header contains JWT headers
type Header struct {
	Algo string `json:"alg"`
	Type string `json:"typ"`
}

// Payload combines all the Claims used in Civic Hosted Services
type Payload struct {
	ID        string      `json:"jti"`
	IssuedAt  Timestamp   `json:"iat"`
	ExpiresAt Timestamp   `json:"exp"`
	Issuer    string      `json:"iss"`
	Audience  string      `json:"aud"`
	Subject   string      `json:"sub"`
	Data      interface{} `json:"data"`
}

// FromString loads an Civic JWT from a string
func FromString(token string) (*Token, error) {
	if strings.Count(token, ".") != 2 {
		return nil, errors.New("invalid number of components")
	}
	parts := strings.Split(token, ".")
	return &Token{parts: parts}, nil
}

// Create allows to create a Civic jwt token
func Create(
	issuer, audience, subject string,
	expiresIn time.Duration,
	data interface{},
	privateKey *ecdsa.PrivateKey,
) (*Token, error) {
	if !validCurve(privateKey) {
		return nil, ErrInvalidCurve
	}
	header, _ := encodeSegment(Header{
		Algo: "ES256",
		Type: "JWT",
	})
	now := time.Now().UTC()
	payload, err := encodeSegment(Payload{
		ID:        uuid.NewV4().String(),
		IssuedAt:  Timestamp(now),
		ExpiresAt: Timestamp(now.Add(expiresIn)),
		Issuer:    issuer,
		Audience:  audience,
		Subject:   subject,
		Data:      data,
	})
	if err != nil {
		return nil, fmt.Errorf("payload encoding: %s", err.Error())
	}
	body := fmt.Sprintf("%s.%s", header, payload)
	r, s, err := ecdsa.Sign(strings.NewReader(body), privateKey, sha256Hash([]byte(body)))
	var sig [2 * p256KeySize]byte
	copy(sig[:p256KeySize], r.Bytes())
	copy(sig[p256KeySize:], s.Bytes())
	token := fmt.Sprintf("%s.%s", body, base64.RawURLEncoding.EncodeToString(sig[:]))
	return FromString(token)
}

// VerifyAndExtractPayload verifies the token and returns the token payload
func VerifyAndExtractPayload(token string, publicKey *ecdsa.PublicKey) (*Payload, error) {
	tok, err := FromString(token)
	if err != nil {
		return nil, errors.Wrap(err, "invalid JWT string")
	}
	err = tok.Verify(publicKey)
	if err != nil {
		return nil, errors.Wrap(err, "failed to verify JWT")
	}
	return tok.Payload()
}

func (j *Token) String() string {
	return strings.Join(j.parts, ".")
}

// Verify checks the signature validity
func (j *Token) Verify(publicKey *ecdsa.PublicKey) error {
	if !validCurve(publicKey) {
		return ErrInvalidCurve
	}

	header, err := j.header()
	if err != nil {
		return fmt.Errorf("unable to decode JWT header: %s", err)
	}
	if header.Algo != "ES256" || header.Type != "JWT" {
		return fmt.Errorf("invalid JWT header: %+v", header)
	}

	rs, err := j.signature()
	if err != nil {
		return fmt.Errorf("signature error: %s", err)
	}
	if len(rs) != 64 {
		return errors.New("invalid signature length")
	}

	r := big.NewInt(0).SetBytes(rs[:p256KeySize])
	s := big.NewInt(0).SetBytes(rs[p256KeySize:])
	valid := ecdsa.Verify(publicKey, j.hash(), r, s)
	if !valid {
		return errors.New("invalid signature")
	}

	return nil
}

// Payload decodes the payload part of the token
func (j *Token) Payload() (*Payload, error) {
	data, err := base64.RawURLEncoding.DecodeString(j.parts[payloadIdx])
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode base64 payload")
	}
	var payload Payload
	err = json.Unmarshal(data, &payload)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal payload")
	}
	return &payload, nil
}

func (j *Token) header() (*Header, error) {
	hdr := j.parts[headerIdx]
	data, err := base64.RawURLEncoding.DecodeString(hdr)
	if err != nil {
		return nil, errors.Wrap(err, "unable to decode base64")
	}
	var header Header
	err = json.Unmarshal(data, &header)
	if err != nil {
		return nil, errors.Wrap(err, "unable to unmarshal json")
	}
	return &header, nil
}

func (j *Token) signature() ([]byte, error) {
	sig := j.parts[signatureIdx]
	if len(sig) == 0 {
		return nil, errors.New("missing signature")
	}
	return base64.RawURLEncoding.DecodeString(sig)
}

func (j *Token) hash() []byte {
	data := strings.Join(j.parts[headerIdx:payloadIdx+1], ".")
	return sha256Hash([]byte(data))
}

func encodeSegment(data interface{}) ([]byte, error) {
	j, err := json.Marshal(data)
	if err != nil {
		return nil, errors.Wrap(err, "unable to marshal segment")
	}
	encoded := []byte(base64.RawURLEncoding.EncodeToString(j))
	return encoded, err
}

func validCurve(curve elliptic.Curve) bool {
	defer func() {
		recover()
	}()
	return curve.Params().Name == "P-256"
}
