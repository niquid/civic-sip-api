package civic

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"
)

// DoJSON executes an http request and decodes JSON to target
func DoJSON(client *http.Client, req *http.Request, target interface{}) error {
	r, err := client.Do(req)
	if err != nil {
		return errors.Wrap(err, "failed to do request")
	}
	if !(r.StatusCode >= 200 && r.StatusCode < 300) {
		body, _ := ioutil.ReadAll(req.Body)
		return fmt.Errorf("invalid HTTP response: { status: %d, body: %+v }", r.StatusCode, body)
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}
