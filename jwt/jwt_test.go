package jwt

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"fmt"
	"math/big"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"go.niquid.tech/civic-sip-api/ecc"
)

var nistPrivateKeyOctets = []byte{
	0xa3, 0xed, 0x0d, 0xd2, 0x7c, 0xbf, 0xa6, 0x2e, 0x13, 0xe3, 0x40, 0xfb,
	0x3d, 0xbb, 0x86, 0x89, 0x5b, 0x99, 0xd5, 0xfd, 0x33, 0x0a, 0x80, 0xe7,
	0x99, 0xba, 0xff, 0xcb, 0x1d, 0x29, 0xc1, 0x7a,
}
var nistPublicKeyOctets = []byte{
	0x04, 0xa7, 0x7e, 0x5c, 0x9c, 0x01, 0xdf, 0x45, 0x7b, 0xa9, 0x41, 0xe2,
	0x8e, 0x18, 0x7d, 0x3f, 0x53, 0x96, 0x2f, 0x90, 0x38, 0xb5, 0xe4, 0x81,
	0x03, 0x6c, 0xd9, 0xe7, 0xe9, 0xd1, 0xb1, 0x04, 0x7c, 0x22, 0x3c, 0x5b,
	0x3d, 0xb3, 0x0f, 0xb1, 0x2f, 0xf9, 0xf2, 0x6e, 0xb2, 0x29, 0xbb, 0x42,
	0x2e, 0xec, 0xf1, 0xa5, 0xdf, 0x67, 0x6d, 0x91, 0x09, 0x9e, 0x08, 0x1e,
	0x4e, 0xc8, 0x8e, 0xc3, 0x39,
}

func TestFromStringFail(t *testing.T) {
	tok, err := FromString("header.body")
	assert.NotNil(t, err)
	assert.Nil(t, tok)
}

func TestString(t *testing.T) {
	str := "a.b.c"
	tok, err := FromString(str)
	if !assert.Nil(t, err) {
		return
	}
	assert.Equal(t, str, tok.String())
}

func TestHeaderFails(t *testing.T) {
	t.Run("base64", func(t *testing.T) {
		str := "a.b.c"
		tok, err := FromString(str)
		if !assert.Nil(t, err) {
			return
		}
		header, err := tok.header()
		assert.Nil(t, header)
		if assert.Error(t, err) {
			assert.Regexp(t, "base64", err)
		}
	})
	t.Run("unmarshal", func(t *testing.T) {
		str := "eyk.b.c"
		tok, err := FromString(str)
		if !assert.Nil(t, err) {
			return
		}
		header, err := tok.header()
		assert.Nil(t, header)
		assert.Error(t, err)
	})
}

func TestSignatureFails(t *testing.T) {
	str := "eyk.b."
	tok, err := FromString(str)
	if !assert.Nil(t, err) {
		return
	}
	header, err := tok.signature()
	assert.Nil(t, header)
	assert.EqualError(t, err, "missing signature")
}

func TestVerifyFails(t *testing.T) {
	t.Run("invalid curve", func(t *testing.T) {
		invalidKey := ecdsa.PublicKey{
			Curve: elliptic.P224(),
			X:     big.NewInt(1),
			Y:     big.NewInt(2),
		}
		tok, err := FromString("a.b.c")
		if !assert.Nil(t, err) {
			return
		}
		err = tok.Verify(&invalidKey)
		assert.EqualError(t, err, ErrInvalidCurve.Error())
	})

	publicKey := ecc.UnmarshalPublicKey(elliptic.P256(), nistPublicKeyOctets)
	t.Run("decode header", func(t *testing.T) {
		tok, err := FromString("a.b.c")
		if !assert.Nil(t, err) {
			return
		}
		err = tok.Verify(&publicKey)
		if assert.Error(t, err) {
			assert.Regexp(t, "^unable to decode JWT header:", err)
		}
	})
	t.Run("invalid header", func(t *testing.T) {
		tok, err := FromString("e30.b.c")
		if !assert.Nil(t, err) {
			return
		}
		err = tok.Verify(&publicKey)
		if assert.Error(t, err) {
			assert.Regexp(t, "^invalid JWT header:", err)
		}
	})
	validHeader := "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9"
	t.Run("decode signature", func(t *testing.T) {
		tok, err := FromString(fmt.Sprintf("%s.b.c", validHeader))
		if !assert.Nil(t, err) {
			return
		}
		err = tok.Verify(&publicKey)
		if assert.Error(t, err) {
			assert.Regexp(t, "^signature error:", err)
		}
	})
	t.Run("invalid signature length", func(t *testing.T) {
		tok, err := FromString(fmt.Sprintf("%s.b.e30", validHeader))
		if !assert.Nil(t, err) {
			return
		}
		err = tok.Verify(&publicKey)
		assert.EqualError(t, err, "invalid signature length")
	})
	t.Run("invalid signature", func(t *testing.T) {
		invalidSig := "AAECAwQFBgcICQoLDA0ODxAREhMUFRYXGBkaGxwdHh8gISIjJCUmJ" +
			"ygpKissLS4vMDEyMzQ1Njc4OTo7PD0-Pw"
		tok, err := FromString(fmt.Sprintf("%s.b.%s", validHeader, invalidSig))
		if !assert.Nil(t, err) {
			return
		}
		err = tok.Verify(&publicKey)
		assert.EqualError(t, err, "invalid signature")
	})
}

func TestCreateInvalidCurve(t *testing.T) {
	invalidKey := ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{
			Curve: elliptic.P224(), // use a wrong curve
			X:     big.NewInt(1),
			Y:     big.NewInt(2),
		},
		D: big.NewInt(3),
	}
	_, err := Create("Issuer", "Audience", "Subject", time.Minute, "", &invalidKey)
	assert.EqualError(t, err, ErrInvalidCurve.Error())
}

func TestInFull(t *testing.T) {
	publicKey := ecc.UnmarshalPublicKey(elliptic.P256(), nistPublicKeyOctets)
	privateKey := ecc.UnmarshalPrivateKey(elliptic.P256(), nistPrivateKeyOctets)
	data := map[string]interface{}{
		"codeToken": 123.0,
	}

	t.Run("create and verify", func(t *testing.T) {
		tok, err := Create("Issuer", "Audience", "Subject", time.Minute, data, &privateKey)
		if !assert.Nil(t, err) {
			return
		}
		err = tok.Verify(&publicKey)
		assert.NoError(t, err)
	})

	t.Run("create, verify and extract payload", func(t *testing.T) {
		tok, err := Create("Issuer", "Audience", "Subject", time.Minute, data, &privateKey)
		if !assert.Nil(t, err) {
			return
		}
		payload, err := VerifyAndExtractPayload(tok.String(), &publicKey)
		assert.NoError(t, err)
		payloadData, ok := payload.Data.(map[string]interface{})
		if assert.True(t, ok) {
			assert.Equal(t, data, payloadData)
		}
	})
}
