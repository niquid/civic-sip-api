package ecc

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"math/big"
)

// UnmarshalPublicKey decodes encoded public key
func UnmarshalPublicKey(curve elliptic.Curve, data []byte) ecdsa.PublicKey {
	x, y := elliptic.Unmarshal(curve, data)
	return ecdsa.PublicKey{
		Curve: curve,
		X:     x,
		Y:     y,
	}
}

// UnmarshalPrivateKey decodes encoded private key
func UnmarshalPrivateKey(curve elliptic.Curve, data []byte) ecdsa.PrivateKey {
	x, y := curve.ScalarBaseMult(data)
	return ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{
			Curve: curve,
			X:     x,
			Y:     y,
		},
		D: (&big.Int{}).SetBytes(data),
	}
}
