package jwt

import (
	"errors"
	"fmt"
	"time"
)

// Timestamp facilitates encoding time to JWT UNIX timestamp
type Timestamp time.Time

// MarshalJSON allows to encode time to float UNIX timestamp
func (t Timestamp) MarshalJSON() ([]byte, error) {
	seconds := float64(time.Time(t).UTC().UnixNano()) / 1e9
	return []byte(fmt.Sprintf("%.3f", seconds)), nil
}

// UnmarshalJSON allows to encode time to float UNIX timestamp
func (t *Timestamp) UnmarshalJSON(data []byte) error {
	var f float64
	n, err := fmt.Sscanf(string(data), "%f", &f)
	if n != 1 {
		return errors.New("unable to decode timestamp")
	}
	if err != nil {
		return err
	}

	secs := int64(f)
	nsecs := int64((f - float64(secs)) * 1e9)
	*t = Timestamp(time.Unix(secs, nsecs))
	return nil
}
