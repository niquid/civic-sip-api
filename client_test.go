package civic

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	appID               = "HyhaWO1SG"
	secretHex           = "44bbae32d1e02bf481074177002bbdef"
	privateKeyNistHex   = "bf5efd7bdde29dc28443614bfee78c3d6ee39c71e55a0437eee02bf7e3647721"
	publicKeyNistHex    = "047d9fd38a4d370d6cff16bf12723e343090d475bf36c1d806b625615a7873b0919f131e38418b0cd5b8a3e0a253fe3a958c7840bfc6be657af68062fecd7943d1"
	processedPayloadURL = "https://h3qe39xpc2.execute-api.us-east-1.amazonaws.com/dev/payload/process?return=url"
)

func TestAll(t *testing.T) {
	authCode := `eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxNzc1ZDQwMi05ZjNjLTQ0OWUtYWZkYS04ZDk4MmM0OGIxYjIiLCJpYXQiOjE1MTk5MzE3MTcuMDM1LCJleHAiOjE1MTk5MzM1MTcuMDM1LCJpc3MiOiJjaXZpYy1zaXAtaG9zdGVkLXNlcnZpY2UiLCJhdWQiOiJodHRwczovL2FwaS5jaXZpYy5jb20vc2lwLyIsInN1YiI6Ikh5aGFXTzFTRyIsImRhdGEiOnsiY29kZVRva2VuIjoiYTRhYjE1MDEtZTg0Ni00NmUyLWEwZDktMzEyNTAwNmIxNzUzIn19.1d3Q3QeL8SE_wlyxHPi6Pn-buf8XsxRlCkfhULiI5CbDLCgEjLuVMGIFSUXg6_snXOD9p-ImVml-0yF-A2-qaw`
	returnData := `eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI1OTYzNWQ2Yy0zYzUyLTQwMzktOTg2OS05MWQwMjUzN2M2YjIiLCJpYXQiOjE1MTk5MzI1NzIuMTU4LCJleHAiOjE1MTk5MzQzNzIuMTU4LCJpc3MiOiJjaXZpYy1zaXAtaG9zdGVkLXNlcnZpY2UiLCJhdWQiOiJodHRwczovL2FwaS5jaXZpYy5jb20vc2lwLyIsInN1YiI6Ikh5aGFXTzFTRyIsImRhdGEiOiI0MDNkNjI0MzY1OTYwMjIyYmQzMWE2MWNhMjQzNWYyY1dOWjhrWkNEUWZWQmtSSVdsbDkzNGhZbDRUTGlrWWVENU52WE0xTUowN2FVQzFtcnFmdVdoWk5qQWVKT1plS0M2emk5Umh3cWR0bkswdWxNRFAwTkRaTHBRa2JqaVdBb1c5RXFYQW41eHNyemZSNUZ0cXZqZ0NORzNvUkp0Y29tRVBvaGVWMDZ3NWZDQ0Z1TjQrbTNiSW5CNldMamNBSmVObUJZT2oyWjFFQVoxcHZ0R2RwSThMWTVYS2VFTHpKM3MzZndidEpXbkorSHFqakxsQjJPM0lmaDBRdVdUMldUNWVrc3RLN1F1bk5MSldiSzJqWkkveGc0RHJFWFl0dnEifQ.YBBljiXaqrbiftAhu6X6csDVbRLcsSNf3xZNRgQzj6Wd7v1Ilja55H_K_gO7zFzj3Qi-bc7-83SI1w6A4Y7MEA`

	privateKeyNist, _ := hex.DecodeString(privateKeyNistHex)
	secret, _ := hex.DecodeString(secretHex)

	cfg := NewConfig(appID, secret, privateKeyNist)
	cfg.Env = "dev"
	cfg.APIBaseURL = "https://kw9lj3a57c.execute-api.us-east-1.amazonaws.com"
	client, _ := NewClient(cfg)

	t.Run("exchange authCode for user data", func(t *testing.T) {
		client.HTTP = NewTestHttpClient(func(req *http.Request) *http.Response {
			assert.Equal(t, "POST", req.Method)
			assert.Equal(t, "https://kw9lj3a57c.execute-api.us-east-1.amazonaws.comdev/scopeRequest/authCode", req.URL.String())

			body := bytes.Buffer{}
			body.ReadFrom(req.Body)
			assert.Equal(t, fmt.Sprintf(`{"allowS3UploadedPayload":true,"authToken":"%s"}`, authCode), body.String())
			userID := "0eb98e188597a61ee90969a42555ded28dcdddccc6ffa8d8023d8833b0a10991"
			data := fmt.Sprintf(`{"data":"%s","userId":"%s","encrypted":true,"alg":"aes"}`, returnData, userID)
			return &http.Response{
				StatusCode: 200,
				Body:       ioutil.NopCloser(bytes.NewBufferString(data)),
				Header:     make(http.Header),
			}
		})

		res, err := client.ExchangeCode(authCode)
		assert.NoError(t, err)
		assert.NotNil(t, res)
	})

	t.Run("wrong response status", func(t *testing.T) {
		client.HTTP = NewTestHttpClient(func(req *http.Request) *http.Response {
			assert.Equal(t, "POST", req.Method)
			assert.Equal(t, "https://kw9lj3a57c.execute-api.us-east-1.amazonaws.comdev/scopeRequest/authCode", req.URL.String())

			body := bytes.Buffer{}
			body.ReadFrom(req.Body)
			assert.Equal(t, fmt.Sprintf(`{"allowS3UploadedPayload":true,"authToken":"%s"}`, authCode), body.String())
			userID := "0eb98e188597a61ee90969a42555ded28dcdddccc6ffa8d8023d8833b0a10991"
			data := fmt.Sprintf(`{"data":"%s","userId":"%s","encrypted":true,"alg":"aes"}`, returnData, userID)
			return &http.Response{
				StatusCode: 401,
				Body:       ioutil.NopCloser(bytes.NewBufferString(data)),
				Header:     make(http.Header),
			}
		})

		res, err := client.ExchangeCode(authCode)
		assert.Nil(t, res)
		assert.EqualError(t, err, "failed to exchange code for user record: failed to get json: invalid HTTP response: { status: 401, body: [] }")
	})

	t.Run("processed (integration)", func(t *testing.T) {
		c := http.Client{}
		client.HTTP = &c

		req, err := http.NewRequest(
			"POST",
			processedPayloadURL,
			strings.NewReader(fmt.Sprintf(`{"payload":"%s"}`, returnData)),
		)
		if !assert.NoError(t, err) {
			return
		}
		req.Header.Set("Content-type", "application/json")
		var payloadResp struct {
			URL string `json:"payloadUrl"`
		}
		err = DoJSON(&c, req, &payloadResp)
		if !assert.NoError(t, err) {
			return
		}
		data := ScopeRequestData{
			Data:      payloadResp.URL,
			Processed: true,
			Encrypted: true,
		}

		err = client.fetchProcessedData(&data)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotEmpty(t, data.Data)
		record, err := client.ExtractUserRecord(&data)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotNil(t, record)
	})
}
