package civic

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"sync"

	"go.niquid.tech/civic-sip-api/ecc"
)

// BaseURL is the address of the Civic Hosted Servies
const BaseURL = "https://api.civic.com/sip/"

// Config Civic SIP client configuration and
type Config struct {
	AppID      string
	AppSecret  []byte
	PrivateKey ecdsa.PrivateKey

	Env          string
	APIBaseURL   string
	SIPPublicKey ecdsa.PublicKey
}

// NewConfig makes a production env configuration for the given appID, appSecret and privateKey
func NewConfig(appID string, appSecret, privateKey []byte) Config {
	cfg := Config{
		AppID:      appID,
		AppSecret:  appSecret,
		Env:        "prod",
		APIBaseURL: BaseURL,
	}
	cfg.PrivateKey = ecc.UnmarshalPrivateKey(elliptic.P256(), privateKey)
	cfg.SIPPublicKey = SIPPublicKey()
	return cfg
}

var pubKeyInit sync.Once
var pubKey ecdsa.PublicKey

// SIPPublicKey returns the public key for the Civic SIP API service
func SIPPublicKey() ecdsa.PublicKey {
	pubKeyInit.Do(func() {
		keyOctets := []byte{
			4, 154, 69, 153, 134, 56, 207, 179, 196, 178, 17, 215, 32, 48, 217,
			174, 131, 41, 162, 66, 219, 99, 191, 176, 7, 106, 84, 231, 100,
			115, 112, 168, 172, 87, 8, 181, 122, 246, 6, 88, 5, 213, 166, 190,
			114, 51, 38, 32, 147, 45, 187, 53, 232, 211, 24, 252, 225, 142,
			124, 152, 10, 14, 178, 106, 161,
		}
		pubKey = ecc.UnmarshalPublicKey(elliptic.P256(), keyOctets)
	})
	return pubKey
}
