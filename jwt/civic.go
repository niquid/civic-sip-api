package jwt

import (
	"encoding/base64"
	"encoding/hex"
)

// CreateCivicExt makes an token extension for the authorization header
func CreateCivicExt(body, secret []byte) string {
	// Civic uses hex-encoded appSecret for the HMAC authentication
	hexSecret := []byte(hex.EncodeToString(secret))

	signature := hmacSha256Hash(body, hexSecret)
	return base64.StdEncoding.EncodeToString(signature)
}
